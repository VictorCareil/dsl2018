/**
 * generated by Xtext 2.15.0
 */
package org.xtext.example.sat.sAT;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.example.sat.sAT.SATPackage#getAnd()
 * @model
 * @generated
 */
public interface And extends BinOp
{
} // And
