/**
 * generated by Xtext 2.15.0
 */
package org.xtext.example.sat.sAT;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primary</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.example.sat.sAT.SATPackage#getPrimary()
 * @model
 * @generated
 */
public interface Primary extends EObject
{
} // Primary
