package org.xtext.example.sat.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.sat.services.SATGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSATParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'('", "')'", "'0'", "'1'", "'!'", "'/\\\\'", "'\\\\/'", "'=>'", "'<=>'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;

    // delegates
    // delegators


        public InternalSATParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSATParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSATParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSAT.g"; }



     	private SATGrammarAccess grammarAccess;

        public InternalSATParser(TokenStream input, SATGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected SATGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalSAT.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalSAT.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalSAT.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalSAT.g:71:1: ruleModel returns [EObject current=null] : this_Primaries_0= rulePrimaries ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject this_Primaries_0 = null;



        	enterRule();

        try {
            // InternalSAT.g:77:2: (this_Primaries_0= rulePrimaries )
            // InternalSAT.g:78:2: this_Primaries_0= rulePrimaries
            {

            		newCompositeNode(grammarAccess.getModelAccess().getPrimariesParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_Primaries_0=rulePrimaries();

            state._fsp--;


            		current = this_Primaries_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRulePrimary"
    // InternalSAT.g:89:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalSAT.g:89:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalSAT.g:90:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalSAT.g:96:1: rulePrimary returns [EObject current=null] : (this_Bool_0= ruleBool | this_Var_1= ruleVar | (otherlv_2= '(' this_Primaries_3= rulePrimaries otherlv_4= ')' ) | (this_Op_5= ruleOp ( (lv_args_6_0= rulePrimary ) ) ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject this_Bool_0 = null;

        EObject this_Var_1 = null;

        EObject this_Primaries_3 = null;

        EObject this_Op_5 = null;

        EObject lv_args_6_0 = null;



        	enterRule();

        try {
            // InternalSAT.g:102:2: ( (this_Bool_0= ruleBool | this_Var_1= ruleVar | (otherlv_2= '(' this_Primaries_3= rulePrimaries otherlv_4= ')' ) | (this_Op_5= ruleOp ( (lv_args_6_0= rulePrimary ) ) ) ) )
            // InternalSAT.g:103:2: (this_Bool_0= ruleBool | this_Var_1= ruleVar | (otherlv_2= '(' this_Primaries_3= rulePrimaries otherlv_4= ')' ) | (this_Op_5= ruleOp ( (lv_args_6_0= rulePrimary ) ) ) )
            {
            // InternalSAT.g:103:2: (this_Bool_0= ruleBool | this_Var_1= ruleVar | (otherlv_2= '(' this_Primaries_3= rulePrimaries otherlv_4= ')' ) | (this_Op_5= ruleOp ( (lv_args_6_0= rulePrimary ) ) ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 13:
            case 14:
                {
                alt1=1;
                }
                break;
            case RULE_ID:
                {
                alt1=2;
                }
                break;
            case 11:
                {
                alt1=3;
                }
                break;
            case 15:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalSAT.g:104:3: this_Bool_0= ruleBool
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getBoolParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Bool_0=ruleBool();

                    state._fsp--;


                    			current = this_Bool_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalSAT.g:113:3: this_Var_1= ruleVar
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getVarParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Var_1=ruleVar();

                    state._fsp--;


                    			current = this_Var_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalSAT.g:122:3: (otherlv_2= '(' this_Primaries_3= rulePrimaries otherlv_4= ')' )
                    {
                    // InternalSAT.g:122:3: (otherlv_2= '(' this_Primaries_3= rulePrimaries otherlv_4= ')' )
                    // InternalSAT.g:123:4: otherlv_2= '(' this_Primaries_3= rulePrimaries otherlv_4= ')'
                    {
                    otherlv_2=(Token)match(input,11,FOLLOW_3); 

                    				newLeafNode(otherlv_2, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_2_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getPrimariesParserRuleCall_2_1());
                    			
                    pushFollow(FOLLOW_4);
                    this_Primaries_3=rulePrimaries();

                    state._fsp--;


                    				current = this_Primaries_3;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_4=(Token)match(input,12,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_2_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalSAT.g:141:3: (this_Op_5= ruleOp ( (lv_args_6_0= rulePrimary ) ) )
                    {
                    // InternalSAT.g:141:3: (this_Op_5= ruleOp ( (lv_args_6_0= rulePrimary ) ) )
                    // InternalSAT.g:142:4: this_Op_5= ruleOp ( (lv_args_6_0= rulePrimary ) )
                    {

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getOpParserRuleCall_3_0());
                    			
                    pushFollow(FOLLOW_3);
                    this_Op_5=ruleOp();

                    state._fsp--;


                    				current = this_Op_5;
                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSAT.g:150:4: ( (lv_args_6_0= rulePrimary ) )
                    // InternalSAT.g:151:5: (lv_args_6_0= rulePrimary )
                    {
                    // InternalSAT.g:151:5: (lv_args_6_0= rulePrimary )
                    // InternalSAT.g:152:6: lv_args_6_0= rulePrimary
                    {

                    						newCompositeNode(grammarAccess.getPrimaryAccess().getArgsPrimaryParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_args_6_0=rulePrimary();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPrimaryRule());
                    						}
                    						set(
                    							current,
                    							"args",
                    							lv_args_6_0,
                    							"org.xtext.example.sat.SAT.Primary");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRulePrimaries"
    // InternalSAT.g:174:1: entryRulePrimaries returns [EObject current=null] : iv_rulePrimaries= rulePrimaries EOF ;
    public final EObject entryRulePrimaries() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimaries = null;


        try {
            // InternalSAT.g:174:50: (iv_rulePrimaries= rulePrimaries EOF )
            // InternalSAT.g:175:2: iv_rulePrimaries= rulePrimaries EOF
            {
             newCompositeNode(grammarAccess.getPrimariesRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimaries=rulePrimaries();

            state._fsp--;

             current =iv_rulePrimaries; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimaries"


    // $ANTLR start "rulePrimaries"
    // InternalSAT.g:181:1: rulePrimaries returns [EObject current=null] : ( ( (lv_name_0_0= rulePrimary ) ) ( ( (lv_name_1_0= ruleBinOp ) ) ( (lv_name_2_0= rulePrimary ) ) )* ) ;
    public final EObject rulePrimaries() throws RecognitionException {
        EObject current = null;

        EObject lv_name_0_0 = null;

        EObject lv_name_1_0 = null;

        EObject lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalSAT.g:187:2: ( ( ( (lv_name_0_0= rulePrimary ) ) ( ( (lv_name_1_0= ruleBinOp ) ) ( (lv_name_2_0= rulePrimary ) ) )* ) )
            // InternalSAT.g:188:2: ( ( (lv_name_0_0= rulePrimary ) ) ( ( (lv_name_1_0= ruleBinOp ) ) ( (lv_name_2_0= rulePrimary ) ) )* )
            {
            // InternalSAT.g:188:2: ( ( (lv_name_0_0= rulePrimary ) ) ( ( (lv_name_1_0= ruleBinOp ) ) ( (lv_name_2_0= rulePrimary ) ) )* )
            // InternalSAT.g:189:3: ( (lv_name_0_0= rulePrimary ) ) ( ( (lv_name_1_0= ruleBinOp ) ) ( (lv_name_2_0= rulePrimary ) ) )*
            {
            // InternalSAT.g:189:3: ( (lv_name_0_0= rulePrimary ) )
            // InternalSAT.g:190:4: (lv_name_0_0= rulePrimary )
            {
            // InternalSAT.g:190:4: (lv_name_0_0= rulePrimary )
            // InternalSAT.g:191:5: lv_name_0_0= rulePrimary
            {

            					newCompositeNode(grammarAccess.getPrimariesAccess().getNamePrimaryParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_5);
            lv_name_0_0=rulePrimary();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPrimariesRule());
            					}
            					add(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.xtext.example.sat.SAT.Primary");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSAT.g:208:3: ( ( (lv_name_1_0= ruleBinOp ) ) ( (lv_name_2_0= rulePrimary ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=16 && LA2_0<=19)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalSAT.g:209:4: ( (lv_name_1_0= ruleBinOp ) ) ( (lv_name_2_0= rulePrimary ) )
            	    {
            	    // InternalSAT.g:209:4: ( (lv_name_1_0= ruleBinOp ) )
            	    // InternalSAT.g:210:5: (lv_name_1_0= ruleBinOp )
            	    {
            	    // InternalSAT.g:210:5: (lv_name_1_0= ruleBinOp )
            	    // InternalSAT.g:211:6: lv_name_1_0= ruleBinOp
            	    {

            	    						newCompositeNode(grammarAccess.getPrimariesAccess().getNameBinOpParserRuleCall_1_0_0());
            	    					
            	    pushFollow(FOLLOW_3);
            	    lv_name_1_0=ruleBinOp();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPrimariesRule());
            	    						}
            	    						add(
            	    							current,
            	    							"name",
            	    							lv_name_1_0,
            	    							"org.xtext.example.sat.SAT.BinOp");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalSAT.g:228:4: ( (lv_name_2_0= rulePrimary ) )
            	    // InternalSAT.g:229:5: (lv_name_2_0= rulePrimary )
            	    {
            	    // InternalSAT.g:229:5: (lv_name_2_0= rulePrimary )
            	    // InternalSAT.g:230:6: lv_name_2_0= rulePrimary
            	    {

            	    						newCompositeNode(grammarAccess.getPrimariesAccess().getNamePrimaryParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_name_2_0=rulePrimary();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPrimariesRule());
            	    						}
            	    						add(
            	    							current,
            	    							"name",
            	    							lv_name_2_0,
            	    							"org.xtext.example.sat.SAT.Primary");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimaries"


    // $ANTLR start "entryRuleBool"
    // InternalSAT.g:252:1: entryRuleBool returns [EObject current=null] : iv_ruleBool= ruleBool EOF ;
    public final EObject entryRuleBool() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBool = null;


        try {
            // InternalSAT.g:252:45: (iv_ruleBool= ruleBool EOF )
            // InternalSAT.g:253:2: iv_ruleBool= ruleBool EOF
            {
             newCompositeNode(grammarAccess.getBoolRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBool=ruleBool();

            state._fsp--;

             current =iv_ruleBool; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBool"


    // $ANTLR start "ruleBool"
    // InternalSAT.g:259:1: ruleBool returns [EObject current=null] : (this_Zero_0= ruleZero | this_One_1= ruleOne ) ;
    public final EObject ruleBool() throws RecognitionException {
        EObject current = null;

        EObject this_Zero_0 = null;

        EObject this_One_1 = null;



        	enterRule();

        try {
            // InternalSAT.g:265:2: ( (this_Zero_0= ruleZero | this_One_1= ruleOne ) )
            // InternalSAT.g:266:2: (this_Zero_0= ruleZero | this_One_1= ruleOne )
            {
            // InternalSAT.g:266:2: (this_Zero_0= ruleZero | this_One_1= ruleOne )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            else if ( (LA3_0==14) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalSAT.g:267:3: this_Zero_0= ruleZero
                    {

                    			newCompositeNode(grammarAccess.getBoolAccess().getZeroParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Zero_0=ruleZero();

                    state._fsp--;


                    			current = this_Zero_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalSAT.g:276:3: this_One_1= ruleOne
                    {

                    			newCompositeNode(grammarAccess.getBoolAccess().getOneParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_One_1=ruleOne();

                    state._fsp--;


                    			current = this_One_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBool"


    // $ANTLR start "entryRuleZero"
    // InternalSAT.g:288:1: entryRuleZero returns [EObject current=null] : iv_ruleZero= ruleZero EOF ;
    public final EObject entryRuleZero() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleZero = null;


        try {
            // InternalSAT.g:288:45: (iv_ruleZero= ruleZero EOF )
            // InternalSAT.g:289:2: iv_ruleZero= ruleZero EOF
            {
             newCompositeNode(grammarAccess.getZeroRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleZero=ruleZero();

            state._fsp--;

             current =iv_ruleZero; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleZero"


    // $ANTLR start "ruleZero"
    // InternalSAT.g:295:1: ruleZero returns [EObject current=null] : ( (lv_name_0_0= '0' ) ) ;
    public final EObject ruleZero() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSAT.g:301:2: ( ( (lv_name_0_0= '0' ) ) )
            // InternalSAT.g:302:2: ( (lv_name_0_0= '0' ) )
            {
            // InternalSAT.g:302:2: ( (lv_name_0_0= '0' ) )
            // InternalSAT.g:303:3: (lv_name_0_0= '0' )
            {
            // InternalSAT.g:303:3: (lv_name_0_0= '0' )
            // InternalSAT.g:304:4: lv_name_0_0= '0'
            {
            lv_name_0_0=(Token)match(input,13,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getZeroAccess().getName0Keyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getZeroRule());
            				}
            				setWithLastConsumed(current, "name", lv_name_0_0, "0");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleZero"


    // $ANTLR start "entryRuleOne"
    // InternalSAT.g:319:1: entryRuleOne returns [EObject current=null] : iv_ruleOne= ruleOne EOF ;
    public final EObject entryRuleOne() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOne = null;


        try {
            // InternalSAT.g:319:44: (iv_ruleOne= ruleOne EOF )
            // InternalSAT.g:320:2: iv_ruleOne= ruleOne EOF
            {
             newCompositeNode(grammarAccess.getOneRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOne=ruleOne();

            state._fsp--;

             current =iv_ruleOne; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOne"


    // $ANTLR start "ruleOne"
    // InternalSAT.g:326:1: ruleOne returns [EObject current=null] : ( (lv_name_0_0= '1' ) ) ;
    public final EObject ruleOne() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSAT.g:332:2: ( ( (lv_name_0_0= '1' ) ) )
            // InternalSAT.g:333:2: ( (lv_name_0_0= '1' ) )
            {
            // InternalSAT.g:333:2: ( (lv_name_0_0= '1' ) )
            // InternalSAT.g:334:3: (lv_name_0_0= '1' )
            {
            // InternalSAT.g:334:3: (lv_name_0_0= '1' )
            // InternalSAT.g:335:4: lv_name_0_0= '1'
            {
            lv_name_0_0=(Token)match(input,14,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getOneAccess().getName1Keyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getOneRule());
            				}
            				setWithLastConsumed(current, "name", lv_name_0_0, "1");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOne"


    // $ANTLR start "entryRuleVar"
    // InternalSAT.g:350:1: entryRuleVar returns [EObject current=null] : iv_ruleVar= ruleVar EOF ;
    public final EObject entryRuleVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVar = null;


        try {
            // InternalSAT.g:350:44: (iv_ruleVar= ruleVar EOF )
            // InternalSAT.g:351:2: iv_ruleVar= ruleVar EOF
            {
             newCompositeNode(grammarAccess.getVarRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVar=ruleVar();

            state._fsp--;

             current =iv_ruleVar; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVar"


    // $ANTLR start "ruleVar"
    // InternalSAT.g:357:1: ruleVar returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleVar() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSAT.g:363:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSAT.g:364:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSAT.g:364:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSAT.g:365:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSAT.g:365:3: (lv_name_0_0= RULE_ID )
            // InternalSAT.g:366:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getVarAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getVarRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVar"


    // $ANTLR start "entryRuleOp"
    // InternalSAT.g:385:1: entryRuleOp returns [EObject current=null] : iv_ruleOp= ruleOp EOF ;
    public final EObject entryRuleOp() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOp = null;


        try {
            // InternalSAT.g:385:43: (iv_ruleOp= ruleOp EOF )
            // InternalSAT.g:386:2: iv_ruleOp= ruleOp EOF
            {
             newCompositeNode(grammarAccess.getOpRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOp=ruleOp();

            state._fsp--;

             current =iv_ruleOp; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOp"


    // $ANTLR start "ruleOp"
    // InternalSAT.g:392:1: ruleOp returns [EObject current=null] : this_Not_0= ruleNot ;
    public final EObject ruleOp() throws RecognitionException {
        EObject current = null;

        EObject this_Not_0 = null;



        	enterRule();

        try {
            // InternalSAT.g:398:2: (this_Not_0= ruleNot )
            // InternalSAT.g:399:2: this_Not_0= ruleNot
            {

            		newCompositeNode(grammarAccess.getOpAccess().getNotParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_Not_0=ruleNot();

            state._fsp--;


            		current = this_Not_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOp"


    // $ANTLR start "entryRuleNot"
    // InternalSAT.g:410:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // InternalSAT.g:410:44: (iv_ruleNot= ruleNot EOF )
            // InternalSAT.g:411:2: iv_ruleNot= ruleNot EOF
            {
             newCompositeNode(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNot=ruleNot();

            state._fsp--;

             current =iv_ruleNot; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalSAT.g:417:1: ruleNot returns [EObject current=null] : ( (lv_name_0_0= '!' ) ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSAT.g:423:2: ( ( (lv_name_0_0= '!' ) ) )
            // InternalSAT.g:424:2: ( (lv_name_0_0= '!' ) )
            {
            // InternalSAT.g:424:2: ( (lv_name_0_0= '!' ) )
            // InternalSAT.g:425:3: (lv_name_0_0= '!' )
            {
            // InternalSAT.g:425:3: (lv_name_0_0= '!' )
            // InternalSAT.g:426:4: lv_name_0_0= '!'
            {
            lv_name_0_0=(Token)match(input,15,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getNotAccess().getNameExclamationMarkKeyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getNotRule());
            				}
            				setWithLastConsumed(current, "name", lv_name_0_0, "!");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRuleBinOp"
    // InternalSAT.g:441:1: entryRuleBinOp returns [EObject current=null] : iv_ruleBinOp= ruleBinOp EOF ;
    public final EObject entryRuleBinOp() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinOp = null;


        try {
            // InternalSAT.g:441:46: (iv_ruleBinOp= ruleBinOp EOF )
            // InternalSAT.g:442:2: iv_ruleBinOp= ruleBinOp EOF
            {
             newCompositeNode(grammarAccess.getBinOpRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBinOp=ruleBinOp();

            state._fsp--;

             current =iv_ruleBinOp; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinOp"


    // $ANTLR start "ruleBinOp"
    // InternalSAT.g:448:1: ruleBinOp returns [EObject current=null] : (this_And_0= ruleAnd | this_Or_1= ruleOr | this_Implies_2= ruleImplies | this_Biimplies_3= ruleBiimplies ) ;
    public final EObject ruleBinOp() throws RecognitionException {
        EObject current = null;

        EObject this_And_0 = null;

        EObject this_Or_1 = null;

        EObject this_Implies_2 = null;

        EObject this_Biimplies_3 = null;



        	enterRule();

        try {
            // InternalSAT.g:454:2: ( (this_And_0= ruleAnd | this_Or_1= ruleOr | this_Implies_2= ruleImplies | this_Biimplies_3= ruleBiimplies ) )
            // InternalSAT.g:455:2: (this_And_0= ruleAnd | this_Or_1= ruleOr | this_Implies_2= ruleImplies | this_Biimplies_3= ruleBiimplies )
            {
            // InternalSAT.g:455:2: (this_And_0= ruleAnd | this_Or_1= ruleOr | this_Implies_2= ruleImplies | this_Biimplies_3= ruleBiimplies )
            int alt4=4;
            switch ( input.LA(1) ) {
            case 16:
                {
                alt4=1;
                }
                break;
            case 17:
                {
                alt4=2;
                }
                break;
            case 18:
                {
                alt4=3;
                }
                break;
            case 19:
                {
                alt4=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalSAT.g:456:3: this_And_0= ruleAnd
                    {

                    			newCompositeNode(grammarAccess.getBinOpAccess().getAndParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_And_0=ruleAnd();

                    state._fsp--;


                    			current = this_And_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalSAT.g:465:3: this_Or_1= ruleOr
                    {

                    			newCompositeNode(grammarAccess.getBinOpAccess().getOrParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Or_1=ruleOr();

                    state._fsp--;


                    			current = this_Or_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalSAT.g:474:3: this_Implies_2= ruleImplies
                    {

                    			newCompositeNode(grammarAccess.getBinOpAccess().getImpliesParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Implies_2=ruleImplies();

                    state._fsp--;


                    			current = this_Implies_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalSAT.g:483:3: this_Biimplies_3= ruleBiimplies
                    {

                    			newCompositeNode(grammarAccess.getBinOpAccess().getBiimpliesParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Biimplies_3=ruleBiimplies();

                    state._fsp--;


                    			current = this_Biimplies_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinOp"


    // $ANTLR start "entryRuleAnd"
    // InternalSAT.g:495:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalSAT.g:495:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalSAT.g:496:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalSAT.g:502:1: ruleAnd returns [EObject current=null] : ( (lv_name_0_0= '/\\\\' ) ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSAT.g:508:2: ( ( (lv_name_0_0= '/\\\\' ) ) )
            // InternalSAT.g:509:2: ( (lv_name_0_0= '/\\\\' ) )
            {
            // InternalSAT.g:509:2: ( (lv_name_0_0= '/\\\\' ) )
            // InternalSAT.g:510:3: (lv_name_0_0= '/\\\\' )
            {
            // InternalSAT.g:510:3: (lv_name_0_0= '/\\\\' )
            // InternalSAT.g:511:4: lv_name_0_0= '/\\\\'
            {
            lv_name_0_0=(Token)match(input,16,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getAndAccess().getNameSolidusReverseSolidusKeyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getAndRule());
            				}
            				setWithLastConsumed(current, "name", lv_name_0_0, "/\\");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleOr"
    // InternalSAT.g:526:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalSAT.g:526:43: (iv_ruleOr= ruleOr EOF )
            // InternalSAT.g:527:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalSAT.g:533:1: ruleOr returns [EObject current=null] : ( (lv_name_0_0= '\\\\/' ) ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSAT.g:539:2: ( ( (lv_name_0_0= '\\\\/' ) ) )
            // InternalSAT.g:540:2: ( (lv_name_0_0= '\\\\/' ) )
            {
            // InternalSAT.g:540:2: ( (lv_name_0_0= '\\\\/' ) )
            // InternalSAT.g:541:3: (lv_name_0_0= '\\\\/' )
            {
            // InternalSAT.g:541:3: (lv_name_0_0= '\\\\/' )
            // InternalSAT.g:542:4: lv_name_0_0= '\\\\/'
            {
            lv_name_0_0=(Token)match(input,17,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getOrAccess().getNameReverseSolidusSolidusKeyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getOrRule());
            				}
            				setWithLastConsumed(current, "name", lv_name_0_0, "\\/");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleImplies"
    // InternalSAT.g:557:1: entryRuleImplies returns [EObject current=null] : iv_ruleImplies= ruleImplies EOF ;
    public final EObject entryRuleImplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplies = null;


        try {
            // InternalSAT.g:557:48: (iv_ruleImplies= ruleImplies EOF )
            // InternalSAT.g:558:2: iv_ruleImplies= ruleImplies EOF
            {
             newCompositeNode(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImplies=ruleImplies();

            state._fsp--;

             current =iv_ruleImplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalSAT.g:564:1: ruleImplies returns [EObject current=null] : ( (lv_name_0_0= '=>' ) ) ;
    public final EObject ruleImplies() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSAT.g:570:2: ( ( (lv_name_0_0= '=>' ) ) )
            // InternalSAT.g:571:2: ( (lv_name_0_0= '=>' ) )
            {
            // InternalSAT.g:571:2: ( (lv_name_0_0= '=>' ) )
            // InternalSAT.g:572:3: (lv_name_0_0= '=>' )
            {
            // InternalSAT.g:572:3: (lv_name_0_0= '=>' )
            // InternalSAT.g:573:4: lv_name_0_0= '=>'
            {
            lv_name_0_0=(Token)match(input,18,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getImpliesAccess().getNameEqualsSignGreaterThanSignKeyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getImpliesRule());
            				}
            				setWithLastConsumed(current, "name", lv_name_0_0, "=>");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleBiimplies"
    // InternalSAT.g:588:1: entryRuleBiimplies returns [EObject current=null] : iv_ruleBiimplies= ruleBiimplies EOF ;
    public final EObject entryRuleBiimplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBiimplies = null;


        try {
            // InternalSAT.g:588:50: (iv_ruleBiimplies= ruleBiimplies EOF )
            // InternalSAT.g:589:2: iv_ruleBiimplies= ruleBiimplies EOF
            {
             newCompositeNode(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBiimplies=ruleBiimplies();

            state._fsp--;

             current =iv_ruleBiimplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalSAT.g:595:1: ruleBiimplies returns [EObject current=null] : ( (lv_name_0_0= '<=>' ) ) ;
    public final EObject ruleBiimplies() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSAT.g:601:2: ( ( (lv_name_0_0= '<=>' ) ) )
            // InternalSAT.g:602:2: ( (lv_name_0_0= '<=>' ) )
            {
            // InternalSAT.g:602:2: ( (lv_name_0_0= '<=>' ) )
            // InternalSAT.g:603:3: (lv_name_0_0= '<=>' )
            {
            // InternalSAT.g:603:3: (lv_name_0_0= '<=>' )
            // InternalSAT.g:604:4: lv_name_0_0= '<=>'
            {
            lv_name_0_0=(Token)match(input,19,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getBiimpliesAccess().getNameLessThanSignEqualsSignGreaterThanSignKeyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getBiimpliesRule());
            				}
            				setWithLastConsumed(current, "name", lv_name_0_0, "<=>");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBiimplies"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000000000E810L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000000F0002L});

}