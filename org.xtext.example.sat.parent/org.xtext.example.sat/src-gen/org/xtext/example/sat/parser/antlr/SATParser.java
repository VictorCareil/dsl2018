/*
 * generated by Xtext 2.15.0
 */
package org.xtext.example.sat.parser.antlr;

import com.google.inject.Inject;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.xtext.example.sat.parser.antlr.internal.InternalSATParser;
import org.xtext.example.sat.services.SATGrammarAccess;

public class SATParser extends AbstractAntlrParser {

	@Inject
	private SATGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalSATParser createParser(XtextTokenStream stream) {
		return new InternalSATParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "Model";
	}

	public SATGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(SATGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
