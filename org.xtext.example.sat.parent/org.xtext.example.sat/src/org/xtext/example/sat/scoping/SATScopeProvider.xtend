/*
 * generated by Xtext 2.15.0
 */
package org.xtext.example.sat.scoping


/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class SATScopeProvider extends AbstractSATScopeProvider {

}
