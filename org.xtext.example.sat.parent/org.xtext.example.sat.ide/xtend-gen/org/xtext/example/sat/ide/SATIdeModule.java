/**
 * generated by Xtext 2.15.0
 */
package org.xtext.example.sat.ide;

import org.xtext.example.sat.ide.AbstractSATIdeModule;

/**
 * Use this class to register ide components.
 */
@SuppressWarnings("all")
public class SATIdeModule extends AbstractSATIdeModule {
}
