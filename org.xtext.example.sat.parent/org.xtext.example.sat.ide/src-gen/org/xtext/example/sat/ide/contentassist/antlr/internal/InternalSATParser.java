package org.xtext.example.sat.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.sat.services.SATGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSATParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'('", "')'", "'0'", "'1'", "'!'", "'/\\\\'", "'\\\\/'", "'=>'", "'<=>'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;

    // delegates
    // delegators


        public InternalSATParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSATParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSATParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSAT.g"; }


    	private SATGrammarAccess grammarAccess;

    	public void setGrammarAccess(SATGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModel"
    // InternalSAT.g:53:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalSAT.g:54:1: ( ruleModel EOF )
            // InternalSAT.g:55:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalSAT.g:62:1: ruleModel : ( rulePrimaries ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:66:2: ( ( rulePrimaries ) )
            // InternalSAT.g:67:2: ( rulePrimaries )
            {
            // InternalSAT.g:67:2: ( rulePrimaries )
            // InternalSAT.g:68:3: rulePrimaries
            {
             before(grammarAccess.getModelAccess().getPrimariesParserRuleCall()); 
            pushFollow(FOLLOW_2);
            rulePrimaries();

            state._fsp--;

             after(grammarAccess.getModelAccess().getPrimariesParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRulePrimary"
    // InternalSAT.g:78:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalSAT.g:79:1: ( rulePrimary EOF )
            // InternalSAT.g:80:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalSAT.g:87:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:91:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalSAT.g:92:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalSAT.g:92:2: ( ( rule__Primary__Alternatives ) )
            // InternalSAT.g:93:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalSAT.g:94:3: ( rule__Primary__Alternatives )
            // InternalSAT.g:94:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRulePrimaries"
    // InternalSAT.g:103:1: entryRulePrimaries : rulePrimaries EOF ;
    public final void entryRulePrimaries() throws RecognitionException {
        try {
            // InternalSAT.g:104:1: ( rulePrimaries EOF )
            // InternalSAT.g:105:1: rulePrimaries EOF
            {
             before(grammarAccess.getPrimariesRule()); 
            pushFollow(FOLLOW_1);
            rulePrimaries();

            state._fsp--;

             after(grammarAccess.getPrimariesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimaries"


    // $ANTLR start "rulePrimaries"
    // InternalSAT.g:112:1: rulePrimaries : ( ( rule__Primaries__Group__0 ) ) ;
    public final void rulePrimaries() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:116:2: ( ( ( rule__Primaries__Group__0 ) ) )
            // InternalSAT.g:117:2: ( ( rule__Primaries__Group__0 ) )
            {
            // InternalSAT.g:117:2: ( ( rule__Primaries__Group__0 ) )
            // InternalSAT.g:118:3: ( rule__Primaries__Group__0 )
            {
             before(grammarAccess.getPrimariesAccess().getGroup()); 
            // InternalSAT.g:119:3: ( rule__Primaries__Group__0 )
            // InternalSAT.g:119:4: rule__Primaries__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Primaries__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPrimariesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimaries"


    // $ANTLR start "entryRuleBool"
    // InternalSAT.g:128:1: entryRuleBool : ruleBool EOF ;
    public final void entryRuleBool() throws RecognitionException {
        try {
            // InternalSAT.g:129:1: ( ruleBool EOF )
            // InternalSAT.g:130:1: ruleBool EOF
            {
             before(grammarAccess.getBoolRule()); 
            pushFollow(FOLLOW_1);
            ruleBool();

            state._fsp--;

             after(grammarAccess.getBoolRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBool"


    // $ANTLR start "ruleBool"
    // InternalSAT.g:137:1: ruleBool : ( ( rule__Bool__Alternatives ) ) ;
    public final void ruleBool() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:141:2: ( ( ( rule__Bool__Alternatives ) ) )
            // InternalSAT.g:142:2: ( ( rule__Bool__Alternatives ) )
            {
            // InternalSAT.g:142:2: ( ( rule__Bool__Alternatives ) )
            // InternalSAT.g:143:3: ( rule__Bool__Alternatives )
            {
             before(grammarAccess.getBoolAccess().getAlternatives()); 
            // InternalSAT.g:144:3: ( rule__Bool__Alternatives )
            // InternalSAT.g:144:4: rule__Bool__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Bool__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBoolAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBool"


    // $ANTLR start "entryRuleZero"
    // InternalSAT.g:153:1: entryRuleZero : ruleZero EOF ;
    public final void entryRuleZero() throws RecognitionException {
        try {
            // InternalSAT.g:154:1: ( ruleZero EOF )
            // InternalSAT.g:155:1: ruleZero EOF
            {
             before(grammarAccess.getZeroRule()); 
            pushFollow(FOLLOW_1);
            ruleZero();

            state._fsp--;

             after(grammarAccess.getZeroRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleZero"


    // $ANTLR start "ruleZero"
    // InternalSAT.g:162:1: ruleZero : ( ( rule__Zero__NameAssignment ) ) ;
    public final void ruleZero() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:166:2: ( ( ( rule__Zero__NameAssignment ) ) )
            // InternalSAT.g:167:2: ( ( rule__Zero__NameAssignment ) )
            {
            // InternalSAT.g:167:2: ( ( rule__Zero__NameAssignment ) )
            // InternalSAT.g:168:3: ( rule__Zero__NameAssignment )
            {
             before(grammarAccess.getZeroAccess().getNameAssignment()); 
            // InternalSAT.g:169:3: ( rule__Zero__NameAssignment )
            // InternalSAT.g:169:4: rule__Zero__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Zero__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getZeroAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleZero"


    // $ANTLR start "entryRuleOne"
    // InternalSAT.g:178:1: entryRuleOne : ruleOne EOF ;
    public final void entryRuleOne() throws RecognitionException {
        try {
            // InternalSAT.g:179:1: ( ruleOne EOF )
            // InternalSAT.g:180:1: ruleOne EOF
            {
             before(grammarAccess.getOneRule()); 
            pushFollow(FOLLOW_1);
            ruleOne();

            state._fsp--;

             after(grammarAccess.getOneRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOne"


    // $ANTLR start "ruleOne"
    // InternalSAT.g:187:1: ruleOne : ( ( rule__One__NameAssignment ) ) ;
    public final void ruleOne() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:191:2: ( ( ( rule__One__NameAssignment ) ) )
            // InternalSAT.g:192:2: ( ( rule__One__NameAssignment ) )
            {
            // InternalSAT.g:192:2: ( ( rule__One__NameAssignment ) )
            // InternalSAT.g:193:3: ( rule__One__NameAssignment )
            {
             before(grammarAccess.getOneAccess().getNameAssignment()); 
            // InternalSAT.g:194:3: ( rule__One__NameAssignment )
            // InternalSAT.g:194:4: rule__One__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__One__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getOneAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOne"


    // $ANTLR start "entryRuleVar"
    // InternalSAT.g:203:1: entryRuleVar : ruleVar EOF ;
    public final void entryRuleVar() throws RecognitionException {
        try {
            // InternalSAT.g:204:1: ( ruleVar EOF )
            // InternalSAT.g:205:1: ruleVar EOF
            {
             before(grammarAccess.getVarRule()); 
            pushFollow(FOLLOW_1);
            ruleVar();

            state._fsp--;

             after(grammarAccess.getVarRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVar"


    // $ANTLR start "ruleVar"
    // InternalSAT.g:212:1: ruleVar : ( ( rule__Var__NameAssignment ) ) ;
    public final void ruleVar() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:216:2: ( ( ( rule__Var__NameAssignment ) ) )
            // InternalSAT.g:217:2: ( ( rule__Var__NameAssignment ) )
            {
            // InternalSAT.g:217:2: ( ( rule__Var__NameAssignment ) )
            // InternalSAT.g:218:3: ( rule__Var__NameAssignment )
            {
             before(grammarAccess.getVarAccess().getNameAssignment()); 
            // InternalSAT.g:219:3: ( rule__Var__NameAssignment )
            // InternalSAT.g:219:4: rule__Var__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Var__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVarAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVar"


    // $ANTLR start "entryRuleOp"
    // InternalSAT.g:228:1: entryRuleOp : ruleOp EOF ;
    public final void entryRuleOp() throws RecognitionException {
        try {
            // InternalSAT.g:229:1: ( ruleOp EOF )
            // InternalSAT.g:230:1: ruleOp EOF
            {
             before(grammarAccess.getOpRule()); 
            pushFollow(FOLLOW_1);
            ruleOp();

            state._fsp--;

             after(grammarAccess.getOpRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOp"


    // $ANTLR start "ruleOp"
    // InternalSAT.g:237:1: ruleOp : ( ruleNot ) ;
    public final void ruleOp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:241:2: ( ( ruleNot ) )
            // InternalSAT.g:242:2: ( ruleNot )
            {
            // InternalSAT.g:242:2: ( ruleNot )
            // InternalSAT.g:243:3: ruleNot
            {
             before(grammarAccess.getOpAccess().getNotParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getOpAccess().getNotParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOp"


    // $ANTLR start "entryRuleNot"
    // InternalSAT.g:253:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // InternalSAT.g:254:1: ( ruleNot EOF )
            // InternalSAT.g:255:1: ruleNot EOF
            {
             before(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getNotRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalSAT.g:262:1: ruleNot : ( ( rule__Not__NameAssignment ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:266:2: ( ( ( rule__Not__NameAssignment ) ) )
            // InternalSAT.g:267:2: ( ( rule__Not__NameAssignment ) )
            {
            // InternalSAT.g:267:2: ( ( rule__Not__NameAssignment ) )
            // InternalSAT.g:268:3: ( rule__Not__NameAssignment )
            {
             before(grammarAccess.getNotAccess().getNameAssignment()); 
            // InternalSAT.g:269:3: ( rule__Not__NameAssignment )
            // InternalSAT.g:269:4: rule__Not__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Not__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRuleBinOp"
    // InternalSAT.g:278:1: entryRuleBinOp : ruleBinOp EOF ;
    public final void entryRuleBinOp() throws RecognitionException {
        try {
            // InternalSAT.g:279:1: ( ruleBinOp EOF )
            // InternalSAT.g:280:1: ruleBinOp EOF
            {
             before(grammarAccess.getBinOpRule()); 
            pushFollow(FOLLOW_1);
            ruleBinOp();

            state._fsp--;

             after(grammarAccess.getBinOpRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinOp"


    // $ANTLR start "ruleBinOp"
    // InternalSAT.g:287:1: ruleBinOp : ( ( rule__BinOp__Alternatives ) ) ;
    public final void ruleBinOp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:291:2: ( ( ( rule__BinOp__Alternatives ) ) )
            // InternalSAT.g:292:2: ( ( rule__BinOp__Alternatives ) )
            {
            // InternalSAT.g:292:2: ( ( rule__BinOp__Alternatives ) )
            // InternalSAT.g:293:3: ( rule__BinOp__Alternatives )
            {
             before(grammarAccess.getBinOpAccess().getAlternatives()); 
            // InternalSAT.g:294:3: ( rule__BinOp__Alternatives )
            // InternalSAT.g:294:4: rule__BinOp__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__BinOp__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBinOpAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinOp"


    // $ANTLR start "entryRuleAnd"
    // InternalSAT.g:303:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalSAT.g:304:1: ( ruleAnd EOF )
            // InternalSAT.g:305:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalSAT.g:312:1: ruleAnd : ( ( rule__And__NameAssignment ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:316:2: ( ( ( rule__And__NameAssignment ) ) )
            // InternalSAT.g:317:2: ( ( rule__And__NameAssignment ) )
            {
            // InternalSAT.g:317:2: ( ( rule__And__NameAssignment ) )
            // InternalSAT.g:318:3: ( rule__And__NameAssignment )
            {
             before(grammarAccess.getAndAccess().getNameAssignment()); 
            // InternalSAT.g:319:3: ( rule__And__NameAssignment )
            // InternalSAT.g:319:4: rule__And__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__And__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleOr"
    // InternalSAT.g:328:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalSAT.g:329:1: ( ruleOr EOF )
            // InternalSAT.g:330:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalSAT.g:337:1: ruleOr : ( ( rule__Or__NameAssignment ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:341:2: ( ( ( rule__Or__NameAssignment ) ) )
            // InternalSAT.g:342:2: ( ( rule__Or__NameAssignment ) )
            {
            // InternalSAT.g:342:2: ( ( rule__Or__NameAssignment ) )
            // InternalSAT.g:343:3: ( rule__Or__NameAssignment )
            {
             before(grammarAccess.getOrAccess().getNameAssignment()); 
            // InternalSAT.g:344:3: ( rule__Or__NameAssignment )
            // InternalSAT.g:344:4: rule__Or__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Or__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleImplies"
    // InternalSAT.g:353:1: entryRuleImplies : ruleImplies EOF ;
    public final void entryRuleImplies() throws RecognitionException {
        try {
            // InternalSAT.g:354:1: ( ruleImplies EOF )
            // InternalSAT.g:355:1: ruleImplies EOF
            {
             before(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getImpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalSAT.g:362:1: ruleImplies : ( ( rule__Implies__NameAssignment ) ) ;
    public final void ruleImplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:366:2: ( ( ( rule__Implies__NameAssignment ) ) )
            // InternalSAT.g:367:2: ( ( rule__Implies__NameAssignment ) )
            {
            // InternalSAT.g:367:2: ( ( rule__Implies__NameAssignment ) )
            // InternalSAT.g:368:3: ( rule__Implies__NameAssignment )
            {
             before(grammarAccess.getImpliesAccess().getNameAssignment()); 
            // InternalSAT.g:369:3: ( rule__Implies__NameAssignment )
            // InternalSAT.g:369:4: rule__Implies__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Implies__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleBiimplies"
    // InternalSAT.g:378:1: entryRuleBiimplies : ruleBiimplies EOF ;
    public final void entryRuleBiimplies() throws RecognitionException {
        try {
            // InternalSAT.g:379:1: ( ruleBiimplies EOF )
            // InternalSAT.g:380:1: ruleBiimplies EOF
            {
             before(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalSAT.g:387:1: ruleBiimplies : ( ( rule__Biimplies__NameAssignment ) ) ;
    public final void ruleBiimplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:391:2: ( ( ( rule__Biimplies__NameAssignment ) ) )
            // InternalSAT.g:392:2: ( ( rule__Biimplies__NameAssignment ) )
            {
            // InternalSAT.g:392:2: ( ( rule__Biimplies__NameAssignment ) )
            // InternalSAT.g:393:3: ( rule__Biimplies__NameAssignment )
            {
             before(grammarAccess.getBiimpliesAccess().getNameAssignment()); 
            // InternalSAT.g:394:3: ( rule__Biimplies__NameAssignment )
            // InternalSAT.g:394:4: rule__Biimplies__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalSAT.g:402:1: rule__Primary__Alternatives : ( ( ruleBool ) | ( ruleVar ) | ( ( rule__Primary__Group_2__0 ) ) | ( ( rule__Primary__Group_3__0 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:406:1: ( ( ruleBool ) | ( ruleVar ) | ( ( rule__Primary__Group_2__0 ) ) | ( ( rule__Primary__Group_3__0 ) ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 13:
            case 14:
                {
                alt1=1;
                }
                break;
            case RULE_ID:
                {
                alt1=2;
                }
                break;
            case 11:
                {
                alt1=3;
                }
                break;
            case 15:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalSAT.g:407:2: ( ruleBool )
                    {
                    // InternalSAT.g:407:2: ( ruleBool )
                    // InternalSAT.g:408:3: ruleBool
                    {
                     before(grammarAccess.getPrimaryAccess().getBoolParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleBool();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getBoolParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSAT.g:413:2: ( ruleVar )
                    {
                    // InternalSAT.g:413:2: ( ruleVar )
                    // InternalSAT.g:414:3: ruleVar
                    {
                     before(grammarAccess.getPrimaryAccess().getVarParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleVar();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getVarParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSAT.g:419:2: ( ( rule__Primary__Group_2__0 ) )
                    {
                    // InternalSAT.g:419:2: ( ( rule__Primary__Group_2__0 ) )
                    // InternalSAT.g:420:3: ( rule__Primary__Group_2__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_2()); 
                    // InternalSAT.g:421:3: ( rule__Primary__Group_2__0 )
                    // InternalSAT.g:421:4: rule__Primary__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalSAT.g:425:2: ( ( rule__Primary__Group_3__0 ) )
                    {
                    // InternalSAT.g:425:2: ( ( rule__Primary__Group_3__0 ) )
                    // InternalSAT.g:426:3: ( rule__Primary__Group_3__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_3()); 
                    // InternalSAT.g:427:3: ( rule__Primary__Group_3__0 )
                    // InternalSAT.g:427:4: rule__Primary__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Bool__Alternatives"
    // InternalSAT.g:435:1: rule__Bool__Alternatives : ( ( ruleZero ) | ( ruleOne ) );
    public final void rule__Bool__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:439:1: ( ( ruleZero ) | ( ruleOne ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            else if ( (LA2_0==14) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalSAT.g:440:2: ( ruleZero )
                    {
                    // InternalSAT.g:440:2: ( ruleZero )
                    // InternalSAT.g:441:3: ruleZero
                    {
                     before(grammarAccess.getBoolAccess().getZeroParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleZero();

                    state._fsp--;

                     after(grammarAccess.getBoolAccess().getZeroParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSAT.g:446:2: ( ruleOne )
                    {
                    // InternalSAT.g:446:2: ( ruleOne )
                    // InternalSAT.g:447:3: ruleOne
                    {
                     before(grammarAccess.getBoolAccess().getOneParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleOne();

                    state._fsp--;

                     after(grammarAccess.getBoolAccess().getOneParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool__Alternatives"


    // $ANTLR start "rule__BinOp__Alternatives"
    // InternalSAT.g:456:1: rule__BinOp__Alternatives : ( ( ruleAnd ) | ( ruleOr ) | ( ruleImplies ) | ( ruleBiimplies ) );
    public final void rule__BinOp__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:460:1: ( ( ruleAnd ) | ( ruleOr ) | ( ruleImplies ) | ( ruleBiimplies ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 16:
                {
                alt3=1;
                }
                break;
            case 17:
                {
                alt3=2;
                }
                break;
            case 18:
                {
                alt3=3;
                }
                break;
            case 19:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalSAT.g:461:2: ( ruleAnd )
                    {
                    // InternalSAT.g:461:2: ( ruleAnd )
                    // InternalSAT.g:462:3: ruleAnd
                    {
                     before(grammarAccess.getBinOpAccess().getAndParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleAnd();

                    state._fsp--;

                     after(grammarAccess.getBinOpAccess().getAndParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSAT.g:467:2: ( ruleOr )
                    {
                    // InternalSAT.g:467:2: ( ruleOr )
                    // InternalSAT.g:468:3: ruleOr
                    {
                     before(grammarAccess.getBinOpAccess().getOrParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleOr();

                    state._fsp--;

                     after(grammarAccess.getBinOpAccess().getOrParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSAT.g:473:2: ( ruleImplies )
                    {
                    // InternalSAT.g:473:2: ( ruleImplies )
                    // InternalSAT.g:474:3: ruleImplies
                    {
                     before(grammarAccess.getBinOpAccess().getImpliesParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleImplies();

                    state._fsp--;

                     after(grammarAccess.getBinOpAccess().getImpliesParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalSAT.g:479:2: ( ruleBiimplies )
                    {
                    // InternalSAT.g:479:2: ( ruleBiimplies )
                    // InternalSAT.g:480:3: ruleBiimplies
                    {
                     before(grammarAccess.getBinOpAccess().getBiimpliesParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleBiimplies();

                    state._fsp--;

                     after(grammarAccess.getBinOpAccess().getBiimpliesParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinOp__Alternatives"


    // $ANTLR start "rule__Primary__Group_2__0"
    // InternalSAT.g:489:1: rule__Primary__Group_2__0 : rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 ;
    public final void rule__Primary__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:493:1: ( rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 )
            // InternalSAT.g:494:2: rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1
            {
            pushFollow(FOLLOW_3);
            rule__Primary__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0"


    // $ANTLR start "rule__Primary__Group_2__0__Impl"
    // InternalSAT.g:501:1: rule__Primary__Group_2__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:505:1: ( ( '(' ) )
            // InternalSAT.g:506:1: ( '(' )
            {
            // InternalSAT.g:506:1: ( '(' )
            // InternalSAT.g:507:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_2_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0__Impl"


    // $ANTLR start "rule__Primary__Group_2__1"
    // InternalSAT.g:516:1: rule__Primary__Group_2__1 : rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2 ;
    public final void rule__Primary__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:520:1: ( rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2 )
            // InternalSAT.g:521:2: rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2
            {
            pushFollow(FOLLOW_4);
            rule__Primary__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1"


    // $ANTLR start "rule__Primary__Group_2__1__Impl"
    // InternalSAT.g:528:1: rule__Primary__Group_2__1__Impl : ( rulePrimaries ) ;
    public final void rule__Primary__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:532:1: ( ( rulePrimaries ) )
            // InternalSAT.g:533:1: ( rulePrimaries )
            {
            // InternalSAT.g:533:1: ( rulePrimaries )
            // InternalSAT.g:534:2: rulePrimaries
            {
             before(grammarAccess.getPrimaryAccess().getPrimariesParserRuleCall_2_1()); 
            pushFollow(FOLLOW_2);
            rulePrimaries();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getPrimariesParserRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1__Impl"


    // $ANTLR start "rule__Primary__Group_2__2"
    // InternalSAT.g:543:1: rule__Primary__Group_2__2 : rule__Primary__Group_2__2__Impl ;
    public final void rule__Primary__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:547:1: ( rule__Primary__Group_2__2__Impl )
            // InternalSAT.g:548:2: rule__Primary__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__2"


    // $ANTLR start "rule__Primary__Group_2__2__Impl"
    // InternalSAT.g:554:1: rule__Primary__Group_2__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:558:1: ( ( ')' ) )
            // InternalSAT.g:559:1: ( ')' )
            {
            // InternalSAT.g:559:1: ( ')' )
            // InternalSAT.g:560:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_2_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__2__Impl"


    // $ANTLR start "rule__Primary__Group_3__0"
    // InternalSAT.g:570:1: rule__Primary__Group_3__0 : rule__Primary__Group_3__0__Impl rule__Primary__Group_3__1 ;
    public final void rule__Primary__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:574:1: ( rule__Primary__Group_3__0__Impl rule__Primary__Group_3__1 )
            // InternalSAT.g:575:2: rule__Primary__Group_3__0__Impl rule__Primary__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__Primary__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__0"


    // $ANTLR start "rule__Primary__Group_3__0__Impl"
    // InternalSAT.g:582:1: rule__Primary__Group_3__0__Impl : ( ruleOp ) ;
    public final void rule__Primary__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:586:1: ( ( ruleOp ) )
            // InternalSAT.g:587:1: ( ruleOp )
            {
            // InternalSAT.g:587:1: ( ruleOp )
            // InternalSAT.g:588:2: ruleOp
            {
             before(grammarAccess.getPrimaryAccess().getOpParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOp();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getOpParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__0__Impl"


    // $ANTLR start "rule__Primary__Group_3__1"
    // InternalSAT.g:597:1: rule__Primary__Group_3__1 : rule__Primary__Group_3__1__Impl ;
    public final void rule__Primary__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:601:1: ( rule__Primary__Group_3__1__Impl )
            // InternalSAT.g:602:2: rule__Primary__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__1"


    // $ANTLR start "rule__Primary__Group_3__1__Impl"
    // InternalSAT.g:608:1: rule__Primary__Group_3__1__Impl : ( ( rule__Primary__ArgsAssignment_3_1 ) ) ;
    public final void rule__Primary__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:612:1: ( ( ( rule__Primary__ArgsAssignment_3_1 ) ) )
            // InternalSAT.g:613:1: ( ( rule__Primary__ArgsAssignment_3_1 ) )
            {
            // InternalSAT.g:613:1: ( ( rule__Primary__ArgsAssignment_3_1 ) )
            // InternalSAT.g:614:2: ( rule__Primary__ArgsAssignment_3_1 )
            {
             before(grammarAccess.getPrimaryAccess().getArgsAssignment_3_1()); 
            // InternalSAT.g:615:2: ( rule__Primary__ArgsAssignment_3_1 )
            // InternalSAT.g:615:3: rule__Primary__ArgsAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Primary__ArgsAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getArgsAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__1__Impl"


    // $ANTLR start "rule__Primaries__Group__0"
    // InternalSAT.g:624:1: rule__Primaries__Group__0 : rule__Primaries__Group__0__Impl rule__Primaries__Group__1 ;
    public final void rule__Primaries__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:628:1: ( rule__Primaries__Group__0__Impl rule__Primaries__Group__1 )
            // InternalSAT.g:629:2: rule__Primaries__Group__0__Impl rule__Primaries__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Primaries__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primaries__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__Group__0"


    // $ANTLR start "rule__Primaries__Group__0__Impl"
    // InternalSAT.g:636:1: rule__Primaries__Group__0__Impl : ( ( rule__Primaries__NameAssignment_0 ) ) ;
    public final void rule__Primaries__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:640:1: ( ( ( rule__Primaries__NameAssignment_0 ) ) )
            // InternalSAT.g:641:1: ( ( rule__Primaries__NameAssignment_0 ) )
            {
            // InternalSAT.g:641:1: ( ( rule__Primaries__NameAssignment_0 ) )
            // InternalSAT.g:642:2: ( rule__Primaries__NameAssignment_0 )
            {
             before(grammarAccess.getPrimariesAccess().getNameAssignment_0()); 
            // InternalSAT.g:643:2: ( rule__Primaries__NameAssignment_0 )
            // InternalSAT.g:643:3: rule__Primaries__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Primaries__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPrimariesAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__Group__0__Impl"


    // $ANTLR start "rule__Primaries__Group__1"
    // InternalSAT.g:651:1: rule__Primaries__Group__1 : rule__Primaries__Group__1__Impl ;
    public final void rule__Primaries__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:655:1: ( rule__Primaries__Group__1__Impl )
            // InternalSAT.g:656:2: rule__Primaries__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primaries__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__Group__1"


    // $ANTLR start "rule__Primaries__Group__1__Impl"
    // InternalSAT.g:662:1: rule__Primaries__Group__1__Impl : ( ( rule__Primaries__Group_1__0 )* ) ;
    public final void rule__Primaries__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:666:1: ( ( ( rule__Primaries__Group_1__0 )* ) )
            // InternalSAT.g:667:1: ( ( rule__Primaries__Group_1__0 )* )
            {
            // InternalSAT.g:667:1: ( ( rule__Primaries__Group_1__0 )* )
            // InternalSAT.g:668:2: ( rule__Primaries__Group_1__0 )*
            {
             before(grammarAccess.getPrimariesAccess().getGroup_1()); 
            // InternalSAT.g:669:2: ( rule__Primaries__Group_1__0 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=16 && LA4_0<=19)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalSAT.g:669:3: rule__Primaries__Group_1__0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Primaries__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getPrimariesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__Group__1__Impl"


    // $ANTLR start "rule__Primaries__Group_1__0"
    // InternalSAT.g:678:1: rule__Primaries__Group_1__0 : rule__Primaries__Group_1__0__Impl rule__Primaries__Group_1__1 ;
    public final void rule__Primaries__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:682:1: ( rule__Primaries__Group_1__0__Impl rule__Primaries__Group_1__1 )
            // InternalSAT.g:683:2: rule__Primaries__Group_1__0__Impl rule__Primaries__Group_1__1
            {
            pushFollow(FOLLOW_3);
            rule__Primaries__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primaries__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__Group_1__0"


    // $ANTLR start "rule__Primaries__Group_1__0__Impl"
    // InternalSAT.g:690:1: rule__Primaries__Group_1__0__Impl : ( ( rule__Primaries__NameAssignment_1_0 ) ) ;
    public final void rule__Primaries__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:694:1: ( ( ( rule__Primaries__NameAssignment_1_0 ) ) )
            // InternalSAT.g:695:1: ( ( rule__Primaries__NameAssignment_1_0 ) )
            {
            // InternalSAT.g:695:1: ( ( rule__Primaries__NameAssignment_1_0 ) )
            // InternalSAT.g:696:2: ( rule__Primaries__NameAssignment_1_0 )
            {
             before(grammarAccess.getPrimariesAccess().getNameAssignment_1_0()); 
            // InternalSAT.g:697:2: ( rule__Primaries__NameAssignment_1_0 )
            // InternalSAT.g:697:3: rule__Primaries__NameAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Primaries__NameAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getPrimariesAccess().getNameAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__Group_1__0__Impl"


    // $ANTLR start "rule__Primaries__Group_1__1"
    // InternalSAT.g:705:1: rule__Primaries__Group_1__1 : rule__Primaries__Group_1__1__Impl ;
    public final void rule__Primaries__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:709:1: ( rule__Primaries__Group_1__1__Impl )
            // InternalSAT.g:710:2: rule__Primaries__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primaries__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__Group_1__1"


    // $ANTLR start "rule__Primaries__Group_1__1__Impl"
    // InternalSAT.g:716:1: rule__Primaries__Group_1__1__Impl : ( ( rule__Primaries__NameAssignment_1_1 ) ) ;
    public final void rule__Primaries__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:720:1: ( ( ( rule__Primaries__NameAssignment_1_1 ) ) )
            // InternalSAT.g:721:1: ( ( rule__Primaries__NameAssignment_1_1 ) )
            {
            // InternalSAT.g:721:1: ( ( rule__Primaries__NameAssignment_1_1 ) )
            // InternalSAT.g:722:2: ( rule__Primaries__NameAssignment_1_1 )
            {
             before(grammarAccess.getPrimariesAccess().getNameAssignment_1_1()); 
            // InternalSAT.g:723:2: ( rule__Primaries__NameAssignment_1_1 )
            // InternalSAT.g:723:3: rule__Primaries__NameAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Primaries__NameAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getPrimariesAccess().getNameAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__Group_1__1__Impl"


    // $ANTLR start "rule__Primary__ArgsAssignment_3_1"
    // InternalSAT.g:732:1: rule__Primary__ArgsAssignment_3_1 : ( rulePrimary ) ;
    public final void rule__Primary__ArgsAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:736:1: ( ( rulePrimary ) )
            // InternalSAT.g:737:2: ( rulePrimary )
            {
            // InternalSAT.g:737:2: ( rulePrimary )
            // InternalSAT.g:738:3: rulePrimary
            {
             before(grammarAccess.getPrimaryAccess().getArgsPrimaryParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getArgsPrimaryParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ArgsAssignment_3_1"


    // $ANTLR start "rule__Primaries__NameAssignment_0"
    // InternalSAT.g:747:1: rule__Primaries__NameAssignment_0 : ( rulePrimary ) ;
    public final void rule__Primaries__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:751:1: ( ( rulePrimary ) )
            // InternalSAT.g:752:2: ( rulePrimary )
            {
            // InternalSAT.g:752:2: ( rulePrimary )
            // InternalSAT.g:753:3: rulePrimary
            {
             before(grammarAccess.getPrimariesAccess().getNamePrimaryParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimariesAccess().getNamePrimaryParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__NameAssignment_0"


    // $ANTLR start "rule__Primaries__NameAssignment_1_0"
    // InternalSAT.g:762:1: rule__Primaries__NameAssignment_1_0 : ( ruleBinOp ) ;
    public final void rule__Primaries__NameAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:766:1: ( ( ruleBinOp ) )
            // InternalSAT.g:767:2: ( ruleBinOp )
            {
            // InternalSAT.g:767:2: ( ruleBinOp )
            // InternalSAT.g:768:3: ruleBinOp
            {
             before(grammarAccess.getPrimariesAccess().getNameBinOpParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBinOp();

            state._fsp--;

             after(grammarAccess.getPrimariesAccess().getNameBinOpParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__NameAssignment_1_0"


    // $ANTLR start "rule__Primaries__NameAssignment_1_1"
    // InternalSAT.g:777:1: rule__Primaries__NameAssignment_1_1 : ( rulePrimary ) ;
    public final void rule__Primaries__NameAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:781:1: ( ( rulePrimary ) )
            // InternalSAT.g:782:2: ( rulePrimary )
            {
            // InternalSAT.g:782:2: ( rulePrimary )
            // InternalSAT.g:783:3: rulePrimary
            {
             before(grammarAccess.getPrimariesAccess().getNamePrimaryParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimariesAccess().getNamePrimaryParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primaries__NameAssignment_1_1"


    // $ANTLR start "rule__Zero__NameAssignment"
    // InternalSAT.g:792:1: rule__Zero__NameAssignment : ( ( '0' ) ) ;
    public final void rule__Zero__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:796:1: ( ( ( '0' ) ) )
            // InternalSAT.g:797:2: ( ( '0' ) )
            {
            // InternalSAT.g:797:2: ( ( '0' ) )
            // InternalSAT.g:798:3: ( '0' )
            {
             before(grammarAccess.getZeroAccess().getName0Keyword_0()); 
            // InternalSAT.g:799:3: ( '0' )
            // InternalSAT.g:800:4: '0'
            {
             before(grammarAccess.getZeroAccess().getName0Keyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getZeroAccess().getName0Keyword_0()); 

            }

             after(grammarAccess.getZeroAccess().getName0Keyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Zero__NameAssignment"


    // $ANTLR start "rule__One__NameAssignment"
    // InternalSAT.g:811:1: rule__One__NameAssignment : ( ( '1' ) ) ;
    public final void rule__One__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:815:1: ( ( ( '1' ) ) )
            // InternalSAT.g:816:2: ( ( '1' ) )
            {
            // InternalSAT.g:816:2: ( ( '1' ) )
            // InternalSAT.g:817:3: ( '1' )
            {
             before(grammarAccess.getOneAccess().getName1Keyword_0()); 
            // InternalSAT.g:818:3: ( '1' )
            // InternalSAT.g:819:4: '1'
            {
             before(grammarAccess.getOneAccess().getName1Keyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getOneAccess().getName1Keyword_0()); 

            }

             after(grammarAccess.getOneAccess().getName1Keyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__One__NameAssignment"


    // $ANTLR start "rule__Var__NameAssignment"
    // InternalSAT.g:830:1: rule__Var__NameAssignment : ( RULE_ID ) ;
    public final void rule__Var__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:834:1: ( ( RULE_ID ) )
            // InternalSAT.g:835:2: ( RULE_ID )
            {
            // InternalSAT.g:835:2: ( RULE_ID )
            // InternalSAT.g:836:3: RULE_ID
            {
             before(grammarAccess.getVarAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVarAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var__NameAssignment"


    // $ANTLR start "rule__Not__NameAssignment"
    // InternalSAT.g:845:1: rule__Not__NameAssignment : ( ( '!' ) ) ;
    public final void rule__Not__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:849:1: ( ( ( '!' ) ) )
            // InternalSAT.g:850:2: ( ( '!' ) )
            {
            // InternalSAT.g:850:2: ( ( '!' ) )
            // InternalSAT.g:851:3: ( '!' )
            {
             before(grammarAccess.getNotAccess().getNameExclamationMarkKeyword_0()); 
            // InternalSAT.g:852:3: ( '!' )
            // InternalSAT.g:853:4: '!'
            {
             before(grammarAccess.getNotAccess().getNameExclamationMarkKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getNotAccess().getNameExclamationMarkKeyword_0()); 

            }

             after(grammarAccess.getNotAccess().getNameExclamationMarkKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__NameAssignment"


    // $ANTLR start "rule__And__NameAssignment"
    // InternalSAT.g:864:1: rule__And__NameAssignment : ( ( '/\\\\' ) ) ;
    public final void rule__And__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:868:1: ( ( ( '/\\\\' ) ) )
            // InternalSAT.g:869:2: ( ( '/\\\\' ) )
            {
            // InternalSAT.g:869:2: ( ( '/\\\\' ) )
            // InternalSAT.g:870:3: ( '/\\\\' )
            {
             before(grammarAccess.getAndAccess().getNameSolidusReverseSolidusKeyword_0()); 
            // InternalSAT.g:871:3: ( '/\\\\' )
            // InternalSAT.g:872:4: '/\\\\'
            {
             before(grammarAccess.getAndAccess().getNameSolidusReverseSolidusKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getAndAccess().getNameSolidusReverseSolidusKeyword_0()); 

            }

             after(grammarAccess.getAndAccess().getNameSolidusReverseSolidusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__NameAssignment"


    // $ANTLR start "rule__Or__NameAssignment"
    // InternalSAT.g:883:1: rule__Or__NameAssignment : ( ( '\\\\/' ) ) ;
    public final void rule__Or__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:887:1: ( ( ( '\\\\/' ) ) )
            // InternalSAT.g:888:2: ( ( '\\\\/' ) )
            {
            // InternalSAT.g:888:2: ( ( '\\\\/' ) )
            // InternalSAT.g:889:3: ( '\\\\/' )
            {
             before(grammarAccess.getOrAccess().getNameReverseSolidusSolidusKeyword_0()); 
            // InternalSAT.g:890:3: ( '\\\\/' )
            // InternalSAT.g:891:4: '\\\\/'
            {
             before(grammarAccess.getOrAccess().getNameReverseSolidusSolidusKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getNameReverseSolidusSolidusKeyword_0()); 

            }

             after(grammarAccess.getOrAccess().getNameReverseSolidusSolidusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__NameAssignment"


    // $ANTLR start "rule__Implies__NameAssignment"
    // InternalSAT.g:902:1: rule__Implies__NameAssignment : ( ( '=>' ) ) ;
    public final void rule__Implies__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:906:1: ( ( ( '=>' ) ) )
            // InternalSAT.g:907:2: ( ( '=>' ) )
            {
            // InternalSAT.g:907:2: ( ( '=>' ) )
            // InternalSAT.g:908:3: ( '=>' )
            {
             before(grammarAccess.getImpliesAccess().getNameEqualsSignGreaterThanSignKeyword_0()); 
            // InternalSAT.g:909:3: ( '=>' )
            // InternalSAT.g:910:4: '=>'
            {
             before(grammarAccess.getImpliesAccess().getNameEqualsSignGreaterThanSignKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getNameEqualsSignGreaterThanSignKeyword_0()); 

            }

             after(grammarAccess.getImpliesAccess().getNameEqualsSignGreaterThanSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__NameAssignment"


    // $ANTLR start "rule__Biimplies__NameAssignment"
    // InternalSAT.g:921:1: rule__Biimplies__NameAssignment : ( ( '<=>' ) ) ;
    public final void rule__Biimplies__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSAT.g:925:1: ( ( ( '<=>' ) ) )
            // InternalSAT.g:926:2: ( ( '<=>' ) )
            {
            // InternalSAT.g:926:2: ( ( '<=>' ) )
            // InternalSAT.g:927:3: ( '<=>' )
            {
             before(grammarAccess.getBiimpliesAccess().getNameLessThanSignEqualsSignGreaterThanSignKeyword_0()); 
            // InternalSAT.g:928:3: ( '<=>' )
            // InternalSAT.g:929:4: '<=>'
            {
             before(grammarAccess.getBiimpliesAccess().getNameLessThanSignEqualsSignGreaterThanSignKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getBiimpliesAccess().getNameLessThanSignEqualsSignGreaterThanSignKeyword_0()); 

            }

             after(grammarAccess.getBiimpliesAccess().getNameLessThanSignEqualsSignGreaterThanSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__NameAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000000000E810L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000000F0000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000000000F0002L});

}